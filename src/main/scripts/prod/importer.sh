#!/bin/bash

folder="Add Your Deployment Directory Path Here, i.e. the directory that contains the project's jar file"
cd $folder
DATE_FROM=`cat <Add The Path to a text file called lastrun which contains the last run date of the pipeline in YYYY-MM-DD format>/lastrun`
CP=".:$folder/*:$folder/lib/*"
source <Add The Path to a Bash Script that will Set Your JAVA_HOME to an OpenJDK 11 build, an example file is provided in the project here for reference, please adjust it depending on you environment>/SetJava11ClassPath.sh
export PATH=$JAVA_HOME/bin:$PATH


SCREENOUT="<Add the Path to Your Log Files Directory here>"_`date +"%Y%m%d%H%M%S"`

COMMAND="java -cp $CP uk.ac.ebi.crossref.mongo.Upserter ${DATE_FROM}"


echo "Calling $COMMAND > $SCREENOUT"

$COMMAND > $SCREENOUT
echo "end of process..."
echo `date +"%Y-%m-%d"` > <Add The Path to a text file called lastrun which contains the last run date of the pipeline, the same one in line 5>/lastrun

cat $SCREENOUT | grep -v "DEBUG" | grep -v "INFO" | Mail -s "CrossRef API Fetcher" "<Add Your Notifications Email Here>" | tee /dev/null

exit 0
