package uk.ac.ebi.crossref.model;

public class MongoConfiguration {

    private String mongoDb;
    private String mongoDbCollectionName;

    public String getMongoDb() {
        return mongoDb;
    }

    public void setMongoDb(String mongoDb) {
        this.mongoDb = mongoDb;
    }

    public String getMongoDbCollectionName() {
        return mongoDbCollectionName;
    }

    public void setMongoDbCollectionName(String mongoDbCollectionName) {
        this.mongoDbCollectionName = mongoDbCollectionName;
    }
}
