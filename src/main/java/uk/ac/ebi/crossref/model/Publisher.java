package uk.ac.ebi.crossref.model;

public class Publisher {
    private String repository;
    private String doiPrefix;
    private String versioningPattern;
    private boolean loadAll;
    private boolean loadMaster;
    private boolean dateFromPostedDateParts;
    private String filter;

    public boolean isDateFromPostedDateParts() {
        return dateFromPostedDateParts;
    }

    public void setDateFromPostedDateParts(boolean dateFromPostedDateParts) {
        this.dateFromPostedDateParts = dateFromPostedDateParts;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public boolean isLoadAll() {
        return loadAll;
    }

    public void setLoadAll(boolean loadAll) {
        this.loadAll = loadAll;
    }

    public boolean isLoadMaster() {
        return loadMaster;
    }

    public void setLoadMaster(boolean loadMaster) {
        this.loadMaster = loadMaster;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getDoiPrefix() {
        return doiPrefix;
    }

    public void setDoiPrefix(String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getVersioningPattern() {
        return versioningPattern;
    }

    public void setVersioningPattern(String versioningPattern) {
        this.versioningPattern = versioningPattern;
    }
}

