package uk.ac.ebi.crossref.model;

import java.util.Date;
import java.util.List;


public class Configuration {
    private String version;
    private Date released;
    private MongoConfiguration mongoConfiguration;
    /**
     * Indicates if the code should parse and import documents for all publishers
     */
    private boolean load4AllPublishers;
    private boolean update;
    private CrossrefApiConfig crossrefApiConfig;
    /**
     * List of publishers of which documents will be parsed and imported
     */
    private List<Publisher> publishers;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getReleased() {
        return released;
    }

    public void setReleased(Date released) {
        this.released = released;
    }

    public MongoConfiguration getMongoConfiguration() {
        return mongoConfiguration;
    }

    public void setMongoConfiguration(MongoConfiguration mongoConfiguration) {
        this.mongoConfiguration = mongoConfiguration;
    }

    public boolean isLoad4AllPublishers() {
        return load4AllPublishers;
    }

    public void setLoad4AllPublishers(boolean load4AllPublishers) {
        this.load4AllPublishers = load4AllPublishers;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public CrossrefApiConfig getCrossrefApiConfig() {
        return crossrefApiConfig;
    }

    public void setCrossrefApiConfig(CrossrefApiConfig crossrefApiConfig) {
        this.crossrefApiConfig = crossrefApiConfig;
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }
}