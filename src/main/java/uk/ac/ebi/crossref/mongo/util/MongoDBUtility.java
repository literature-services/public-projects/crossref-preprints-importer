package uk.ac.ebi.crossref.mongo.util;

public class MongoDBUtility {

	public static boolean isStringEmpty(String string){
        return string == null || string.trim().isEmpty();
	}
}
