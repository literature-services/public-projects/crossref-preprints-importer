package uk.ac.ebi.crossref.mongo;

import com.mongodb.bulk.BulkWriteResult;
import org.bson.Document;
import uk.ac.ebi.crossref.common.PropertyManager;
import uk.ac.ebi.crossref.model.Publisher;
import uk.ac.ebi.crossref.mongo.dao.ICrudDAO;
import uk.ac.ebi.crossref.mongo.dao.impl.CrudDaoImpl;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

public class Upserter {

    private static final int PAGE_SIZE = 1000;
    private static int modifiedCount;
    private static int insertedCount;
    private static int totalModifiedCount;
    private static int totalInsertedCount;
    private static final List<String> DOI = List.of("DOI");
    private static final String PREFIX = ",prefix:";
    private static int REQS_LIMIT = 50;
    private static int REQS_INTERVAL = 1000;
    private static PropertyManager propertyManager;
    private static String dateFrom;

    public static void main(String[] args) {
        init(args);
        fetchAndUpsertDocuments();
    }

    private static void init(String[] args) {
        propertyManager = PropertyManager.loadProperties();
        dateFrom = args[0];
    }

    private static void fetchAndUpsertDocuments() {
        try(ICrudDAO dao = new CrudDaoImpl(propertyManager.getMongoDbUrl())){
            propertyManager
                    .getConfig()
                    .getPublishers()
                    .forEach(publisher -> loadDocuments(dao, publisher));

            System.out.println("Total Modified: " + totalModifiedCount);
            System.out.println("Total Inserted: " + totalInsertedCount);
            System.out.println("done");
        }   catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadDocuments(ICrudDAO dao, Publisher publisher)  {
        String cursor = "*";
        boolean done = false;
        long before = System.currentTimeMillis();
        long timePassedBetweenRequests = 0;
        int numOfReqsWithinInterval = 0;
        int sentCalls = 0;
        int totalCalls = 1;
        modifiedCount = 0;
        insertedCount = 0;
        while (!done) {
            String dateFilter =  propertyManager.getConfig().getCrossrefApiConfig().getDateFilter() + dateFrom;
            String doiPrefix = publisher.getDoiPrefix();
            String urlDoiPrefix = (doiPrefix == null || doiPrefix.isEmpty()) ? "" : PREFIX + doiPrefix;
            String documentString =
                    sendGetDocumentRequest(
                            propertyManager.getConfig().getCrossrefApiConfig().getBaseUrl()
                                    + publisher.getFilter()
                                    + dateFilter
                                    + urlDoiPrefix
                                    + "&sort=updated&rows="
                                    + PAGE_SIZE
                                    + "&cursor="
                                    + cursor);
            numOfReqsWithinInterval++;
            long now = System.currentTimeMillis();
            timePassedBetweenRequests = now - before;
            if (timePassedBetweenRequests >= REQS_INTERVAL) {
                numOfReqsWithinInterval = 0;
                before = now;
            }
            try {
                if (numOfReqsWithinInterval > REQS_LIMIT) {
                    System.out.println("Sleeping");
                    sleep(REQS_INTERVAL - timePassedBetweenRequests);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(documentString != null) {
                Document document = Document.parse(documentString);
                Document message = (Document) document.get("message");
                boolean hasNext = message.containsKey("next-cursor");
                String nextCursor;
                nextCursor = hasNext ? URLEncoder.encode((String) message.get("next-cursor"), StandardCharsets.UTF_8) : null;
                sentCalls++;
                int totalResults = (int) message.get("total-results");
                System.out.println("total results: " + totalResults);
                // Calculate the number of total calls, rounding up for any remainder
                totalCalls = (int) Math.ceil((double) totalResults / PAGE_SIZE);
                done = (sentCalls >= totalCalls);
                cursor = nextCursor;
                List<Document> items = (List<Document>) message.get("items");
                upsertItemsIntoMongoDB(dao, publisher, items);
            }
        }
        System.out.println("Done importing " + publisher.getRepository());
        System.out.println("total call: " + totalCalls + ", sent calls: " + sentCalls );
        System.out.println("modified: " + modifiedCount);
        System.out.println("inserted: " + insertedCount);
    }

    private static String sendGetDocumentRequest(String urlQueryString) {
        System.out.println("calling " + urlQueryString);
        String documentString = null;
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(urlQueryString);
            connection = getCrossrefApiHttpsConnection(url);
            Map<String, List<String>> headers = connection.getHeaderFields();
            System.out.println(headers);
            updateRateLimitingParameters(headers);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8));
            documentString = readDocumentString(in);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error during calling " + urlQueryString);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return documentString;
    }

    private static HttpsURLConnection getCrossrefApiHttpsConnection(URL url) throws IOException {
        int connectionTimeout = 90000;
        int readTimeout = 900000;
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("charset", "utf-8");
        connection.setConnectTimeout(connectionTimeout);
        connection.setReadTimeout(readTimeout);
        connection.connect();
        return connection;
    }

    private static void updateRateLimitingParameters(Map<String, List<String>> headers) {
        if (headers.containsKey("x-rate-limit-limit")) {
            REQS_LIMIT = Integer.parseInt(headers.get("x-rate-limit-limit").get(0));
        }
        if (headers.containsKey("x-rate-limit-interval")) {
            int numberOfMilliSecondsInASecond = 1000;
            String intervalString = headers.get("x-rate-limit-interval").get(0);
            int requestsIntervalInSeconds = Integer.parseInt(intervalString.substring(0,intervalString.indexOf("s")));
            REQS_INTERVAL = numberOfMilliSecondsInASecond * requestsIntervalInSeconds;
        }
    }

    private static String readDocumentString(BufferedReader in) throws IOException {
        String readLine;
        StringBuilder documentStringBuilder = new StringBuilder();
        while ((readLine = in.readLine()) != null) {
            documentStringBuilder.append(readLine);
        }
        in.close();
        return documentStringBuilder.toString();
    }

    private static void upsertItemsIntoMongoDB(ICrudDAO dao, Publisher publisher, List<Document> items)  {
        String scienceOpenRepository = "ScienceOpen Preprints";
        boolean isScienceOpen = scienceOpenRepository.equals(publisher.getRepository());
        List<Document> irrelevantScienceOpenItems = new ArrayList<>();
        for (Document item : items) {
            if(isScienceOpen) {
                String publicationSubType = (String) item.get("subtype");
                if(!publicationSubType.equals("preprint")) {
                    irrelevantScienceOpenItems.add(item);
                }
            }
            if ("F1000".equals(publisher.getRepository())) {
                item.append("publisher-by-doi", ((List)item.get("short-container-title")).get(0));
            }
            else if ("CSHL".equals(publisher.getRepository())) {
                item.append("publisher-by-doi", ((List)item.get("institution")).get(0));
            }
            else    {
                item.append("publisher-by-doi", publisher.getRepository());
            }
            item.append("epmc-loaded", new Date(System.currentTimeMillis()));
        }
        if (isScienceOpen) {
            items.removeAll(irrelevantScienceOpenItems);
        }
        try {
            String mongoDbCollectionName =
                    propertyManager
                            .getConfig()
                            .getMongoConfiguration()
                            .getMongoDbCollectionName();
            if (!items.isEmpty()) {
                if (propertyManager.getConfig().isUpdate()) {
                    BulkWriteResult bulkWriteResult = dao.upsertEntries(mongoDbCollectionName, DOI, items);
                    modifiedCount += bulkWriteResult.getModifiedCount();
                    insertedCount += bulkWriteResult.getUpserts().size();
                    totalModifiedCount += modifiedCount;
                    totalInsertedCount += insertedCount;
                }
                else  {
                    dao.insertEntries(mongoDbCollectionName, items);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}