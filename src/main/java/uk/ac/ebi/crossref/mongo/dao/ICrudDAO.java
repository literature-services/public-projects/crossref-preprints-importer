package uk.ac.ebi.crossref.mongo.dao;

import com.mongodb.bulk.BulkWriteResult;
import org.bson.Document;

import java.util.List;

public interface ICrudDAO extends AutoCloseable {
	/**
	 * List of the MONGO db database managed. 
	 * Once that it's necessary to add a database then make the following actions:
	 * <ol>
	 * 
	 * <li>add an enum value with a string associated </li>
	 * <li>Create in the mongodb.properties file the connection properties with the proper name.
	 * Considering for example the enum value LOCALHOST the connection values are:
	 * hostName.localhost=localhost
	 * port.localhost=2017
	 * databaseAuthentication.localhost=[Your authentication mechanism]
	 * userName.localhost=[Your user name]
	 * password.localhost=[Your password]
       
       where localhost is the dbName associated with the enum.
	 If the enum has more than one dbName it means that a list of seeds will be associated with the mongoDb operations</li>
	 * </ol>
	 * 
	 *
	 */
    enum MONGODB_URL{
		/**
		 * localhost instance
		 */
		LOCALHOST(new String[]{"localhost"}, "literature");

		/**
		 * list of names for which to read the connection properties into the mongodb.properties file.
		 * Every element of the array specifies one seed to connect to.
		 */
		private final String[] dbNames;
		
		private final String defaultDbName;

		MONGODB_URL(String[] dbNames, String defaultDbName){
			this.dbNames = dbNames;
			this.defaultDbName = defaultDbName;
			
		}
		
		public String[] getDbNames(){
			return this.dbNames;
		}

		public String getDefaultDbName() {
			return defaultDbName;
		}

	}

	/**
	 * Method to update or insert the documents specified.
	 * @param collection name of the collection to update/insert the documents into
	 * @param fieldNameKey list of the key fields used to match the documents to update. If no match is found for a specific document using the filter specified, a new document will be inserted
	 * @param entities documents to update or insert 
	 * @return BulkWriteResult. It will be null if the operation fails
	 */
    BulkWriteResult upsertEntries(String collection, List<String> fieldNameKey, List<Document> entities);

	/**
	 * Method to insert a list of documents in the collection specified
	 *
	 * @param collection name of the collection to insert the data into
	 * @param documents  list of documents to insert into the database
	 */
    void insertEntries(String collection, List<Document> documents);

	/**
	 * Method to close the connection.
	 * It has to be used in a finally clause every time an instance of this interface is created.
	 */
    void closeConnection();

    /**
     * Getter for The DBUrl connected with this Dao
     * @return MONGODB_URL
     */
    MONGODB_URL getDbUrl();
}
