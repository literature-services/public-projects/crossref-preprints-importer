package uk.ac.ebi.crossref.mongo.dao.impl;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReplaceOptions;
import org.apache.log4j.Logger;
import org.bson.Document;
import uk.ac.ebi.crossref.mongo.dao.ICrudDAO;
import uk.ac.ebi.crossref.mongo.util.MongoDBUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CrudDaoImpl implements ICrudDAO {

	private static final Logger logger = Logger.getLogger(CrudDaoImpl.class);
	
	protected MONGODB_URL dbUrl;

	private final MongoClient mongoClient;
	
	private final MongoDatabase mongoDatabase;

	/**
	 * 
	 * @param dbUrl dbUrl to connect to. The default database is literature
	 * @throws IOException if the mongodb.properties file is not found in the resources folder
	 */
	public CrudDaoImpl (MONGODB_URL dbUrl ) throws IOException{
		this(dbUrl,dbUrl.getDefaultDbName());
	}
	
	/**
	 * 
	 * @param dbUrl dbUrl to connect to. The default database is literature
	 * @param databaseName database to connect to
	 * @throws IOException if the mongodb.properties file is not found in the resources folder
	 */
	public CrudDaoImpl (MONGODB_URL dbUrl, String databaseName ) throws IOException{
        this.dbUrl = dbUrl;
		StringBuilder connectionInformation = new StringBuilder();

		List<MongoCredential> listCredentials = new ArrayList<>();
		ConnectionData connectionData;
		MongoCredential credentialData;
		boolean addCredential;
		List<ConnectionData> connectionDataList = new ArrayList<>();
        for (String dbName : dbUrl.getDbNames()){
        	connectionData = new ConnectionData(dbName);
        	connectionDataList.add(connectionData);
        	connectionInformation
					.append(" ")
					.append(connectionData.getHostName())
					.append(":")
					.append(connectionData.getPort());
        	if (!MongoDBUtility.isStringEmpty(connectionData.getUserName())){
        		addCredential= true;
        		for (MongoCredential credential : listCredentials){
        			if (connectionData.match(credential)){
        				addCredential= false;
        				break;
        			}
        		}
        		
        		if (addCredential){
        			credentialData = MongoCredential.createCredential(connectionData.getUserName(),
        					connectionData.getDatabaseAuthentication(),
        					connectionData.getPassword().toCharArray());
        			listCredentials.add(credentialData);
        		}
        	}
        }
        
        ConnectionString connectionString = getConnectionString(connectionDataList, !listCredentials.isEmpty(), databaseName);
        MongoClientSettings settings =
				MongoClientSettings
						.builder()
						.applyConnectionString(connectionString)
						.build();
        mongoClient = MongoClients.create(settings);
    	mongoDatabase = mongoClient.getDatabase(databaseName);
        String mongoDbConnectionInfo= this.mongoClient.getClusterDescription().getShortDescription();
    	String fullConnectionInformation =
				"Connected to "
						+ this.getDbUrl().name()
						+ " : "
						+ mongoDbConnectionInfo
						+ " ("
						+ connectionInformation
						+ ")";
		logger.info(fullConnectionInformation);
	}
	
	private ConnectionString getConnectionString(List<ConnectionData> connectionData, boolean addCredentials, String databaseName) {
		StringBuilder connection = new StringBuilder("mongodb://");
		
		if (addCredentials) {
			connection.append(connectionData.get(0).getUserName()).append(":").append(connectionData.get(0).getPassword()).append("@");
		}
		
		boolean first = true;
		for (ConnectionData connectionInfo : connectionData) {
			if (!first) {
				connection.append(",");
			}
			connection.append(connectionInfo.getHostName()).append(":").append(connectionInfo.getPort());
			first = false;
		}
		
		connection.append("/").append(databaseName).append("?readPreference=secondaryPreferred");
		if (addCredentials) {
			connection.append("&authSource=").append(connectionData.get(0).getDatabaseAuthentication());
		}
		// handle replica set connection
		if(!connectionData.get(0).getReplicaSet().isEmpty()) {
			connection.append("&replicaSet=").append(connectionData.get(0).getReplicaSet());
		}

        return new ConnectionString(connection.toString());
	}
	
	@Override
	public void closeConnection() {
		this.mongoClient.close();
	}


	@Override
	public BulkWriteResult upsertEntries(String collection, List<String> fieldNameKey, List<Document> entities) {
		BulkWriteResult bulkWriteResult = null;
		try{
			List<ReplaceOneModel<Document>> updateDocuments = new ArrayList<>();
			for (Document updateDocument : entities){

			    //Finder doc
			    Document filterDocument = new Document();
			    for (String key : fieldNameKey){
			    	filterDocument.append(key, updateDocument.get(key));
			    }

			    //Update option
			    ReplaceOptions updateOptions = new ReplaceOptions();
			    updateOptions.upsert(true); //if true, will create a new doc in case of unmatched find
			    updateOptions.bypassDocumentValidation(false); //set true/false

			    //Prepare list of Updates
			    updateDocuments.add(
                        new ReplaceOneModel<>(
                                filterDocument,
                                updateDocument,
                                updateOptions));
			}

			//Bulk write options
			BulkWriteOptions bulkWriteOptions = new BulkWriteOptions();
			bulkWriteOptions.ordered(true);
			bulkWriteOptions.bypassDocumentValidation(false);
			MongoCollection<Document> collectionMongoData = mongoDatabase.getCollection(collection);
			bulkWriteResult = collectionMongoData.bulkWrite(updateDocuments, bulkWriteOptions);
		} catch(Exception e) {
			this.logError(e);
        }
		return bulkWriteResult;
	}

	@Override
	public void insertEntries(String collection, List<Document> documents){
		try{
			MongoCollection<Document> collectionData = mongoDatabase.getCollection(collection);
			collectionData.insertMany(documents);
		}catch(Exception e){
			this.logError(e);
		}
	}


	/**
	 * Method to log an exception related to MongoDB operations
	 * @param e exception occurred during a MongoDB operation execution
	 */
	private void logError(Exception e) {
		logger.error("MongoDB ERROR:" + e.getMessage(), e);
		System.out.println("MongoDB ERROR: " + e);
	}

	public MONGODB_URL getDbUrl() {
		return dbUrl;
	}

	@Override
	public void close() {
		this.closeConnection();
	}
}
