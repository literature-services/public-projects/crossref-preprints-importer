package uk.ac.ebi.crossref.common;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;
import uk.ac.ebi.crossref.model.Configuration;
import uk.ac.ebi.crossref.model.Publisher;
import uk.ac.ebi.crossref.mongo.dao.ICrudDAO;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class PropertyManager {
    private static final Logger logger = Logger.getLogger(PropertyManager.class);

    private final uk.ac.ebi.crossref.mongo.dao.ICrudDAO.MONGODB_URL mongoDbUrl;
    private final Configuration config;

    public Configuration getConfig() {
        return config;
    }

    public PropertyManager(
            ICrudDAO.MONGODB_URL mongoDbUrl,
            Configuration config) {
        this.mongoDbUrl = mongoDbUrl;
        this.config = config;
    }

    public ICrudDAO.MONGODB_URL getMongoDbUrl() {
        return mongoDbUrl;
    }


    public static PropertyManager loadProperties() {
        Yaml yaml = new Yaml();
        try (InputStream in = PropertyManager.class.getClassLoader().getResourceAsStream("application.yml")) {
            Configuration config = yaml.loadAs(in, Configuration.class);
            boolean loadForAllPublishers = config.isLoad4AllPublishers();
            logger.info("### Parse documents for ALL publishers? = " + loadForAllPublishers);
            List<String> publisherList = getPublisherList(config.getPublishers());
            logger.info("### Parse documents for publishers = " + publisherList);
            ICrudDAO.MONGODB_URL mongoDbUrl =
                    ICrudDAO.MONGODB_URL.valueOf(
                            ICrudDAO.MONGODB_URL.class,
                            config.getMongoConfiguration().getMongoDb());
            return new PropertyManager(mongoDbUrl, config);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<String> getPublisherList(List<Publisher> publishers) {
        List<String> publisherList = new ArrayList<>();
        for (Publisher publisher : publishers) {
            publisherList.add(publisher.getRepository() + ":" + publisher.getDoiPrefix());
        }
        return publisherList;
    }
}